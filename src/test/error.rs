use client;

#[derive(Debug)]
pub enum Error {
    ClientError(client::Error),
    Unknown,
}

impl From<client::Error> for Error {
    fn from(err: client::Error) -> Self {
        Error::ClientError(err)
    }
}
