#![allow(dead_code, unused_imports)]

mod error;
pub use self::error::Error;

use std::result;
use client::Client;

type Result<T> = result::Result<T, Error>;
