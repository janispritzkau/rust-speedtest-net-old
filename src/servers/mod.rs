mod error;
mod server;

pub use self::server::Server;
pub use self::error::Error;

use std::result;
use std::io::Read;
use serialize::json;
use client::Client;
use http;

type Result<T> = result::Result<T, Error>;

/// Get a list of servers by the beta.speedtest.net API.
pub fn fetch_servers() -> Result<Vec<Server>> {
    let mut res = try!(http::Client::new().get("http://beta.speedtest.net/api/js/servers").send());
    let mut buf = String::new();
    res.read_to_string(&mut buf).unwrap();
    let servers: Vec<Server> = try!(json::decode(&buf));
    Ok(servers)
}

/// Get the best server based on the latency from a local client.
pub fn get_best_server(servers: &[Server]) -> Result<Server> {
    struct ServerPing {
        server: Server,
        ping: u32
    }

    let mut server_pings = Vec::<ServerPing>::new();
    for server in servers {
        if let Ok(ping) = test_server_ping(&server) {
            server_pings.push(ServerPing {server: server.clone(), ping: ping})
        }
    }
    server_pings.sort_by(|a, b| b.ping.cmp(&a.ping));
    let server = server_pings.first().unwrap().server.clone();
    Ok(server)
}

fn test_server_ping(server: &Server) -> Result<u32> {
    let host = &server.host[..];
    let mut client = try!(Client::connect(host));
    let mut pings = Vec::<u32>::new();
    for _ in 0..5 {
        let ping = try!(client.ping());
        pings.push(ping.ms());
    }
    let lowest = pings.iter().min().unwrap();
    Ok(*lowest)
}
