use serialize::{Decoder, Decodable};

/// A structure which represents a speedtest.net server.
#[derive(Clone, Debug)]
pub struct Server {
    pub name: String,
    pub sponsor: String,
    pub country: String,
    pub url: String,
    pub host: String,
    pub latitude: f64,
    pub longitude: f64,
    pub distance: u64,
}

impl Decodable for Server {
    fn decode<D: Decoder>(d: &mut D) -> Result<Server, D::Error> {
        d.read_struct("Server", 0, |d| {
            let name = try!(d.read_struct_field("name", 0, |d| d.read_str()));
            let sponsor = try!(d.read_struct_field("sponsor", 0, |d| d.read_str()));
            let country = try!(d.read_struct_field("country", 0, |d| d.read_str()));
            let url = try!(d.read_struct_field("url", 0, |d| d.read_str()));
            let host = try!(d.read_struct_field("host", 0, |d| d.read_str()));
            let distance = try!(d.read_struct_field("distance", 0, |d| d.read_u64()));
            let latitude = try!(d.read_struct_field("lat", 0, |d| d.read_f64()));
            let longitude = try!(d.read_struct_field("lon", 0, |d| d.read_f64()));
            Ok(Server {
                name: name, country: country, sponsor: sponsor,
                url: url, host: host,
                distance: distance, longitude: longitude, latitude: latitude
            })
        })
    }
}
