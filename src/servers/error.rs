use serialize::json;
use client;
use http;

#[derive(Debug)]
pub enum Error {
    DecoderError(json::DecoderError),
    ClientError(client::Error),
    HttpError(http::Error),
    Unknown
}

impl From<json::DecoderError> for Error {
    fn from(err: json::DecoderError) -> Error {
        Error::DecoderError(err)
    }
}

impl From<client::Error> for Error {
    fn from(err: client::Error) -> Error {
        Error::ClientError(err)
    }
}

impl From<http::Error> for Error {
    fn from(err: http::Error) -> Error {
        Error::HttpError(err)
    }
}
