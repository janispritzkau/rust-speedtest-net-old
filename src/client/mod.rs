mod client;
mod error;

pub use self::error::Error;
pub use self::client::*;

use std::fmt;
use time::Duration;

/// A structure which represents the latency of a ping test.
pub struct PingLatency {
    latency: Duration
}

impl PingLatency {
    pub fn ms(&self) -> u32 {
        self.latency.num_milliseconds() as u32
    }
}

impl fmt::Debug for PingLatency {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{ms} ms", ms = self.ms())
    }
}

/// A structure which represents the amount of data and time it took to
/// transfer it.
#[derive(Clone, Copy)]
pub struct SpeedMeasurement {
    pub size: usize,
    pub duration: Duration
}

impl SpeedMeasurement {
    pub fn kbit_s(&self) -> u32 {
        self.size as u32 * 8 / self.duration.num_milliseconds() as u32
    }
}

impl fmt::Debug for SpeedMeasurement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:.3} Mbit/s ({size} byte / {s:.3} s)",
            self.kbit_s() as f32 / 1000.0,
            size = self.size,
            s = self.duration.num_milliseconds() as f32 / 1000.0
        )
    }
}
