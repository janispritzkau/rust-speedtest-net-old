use std::result;
use std::io::{Write, BufReader, BufRead};
use std::net::{TcpStream, ToSocketAddrs};
use std::cmp::{min};
use time::now;
use rand::random;

use super::{Error, SpeedMeasurement, PingLatency};

type Result<T> = result::Result<T, Error>;

/// Represents a connection between the local client and the server.
/// It is used to measure the speed of the connection.
pub struct Client {
    reader: BufReader<TcpStream>
}

impl Client {
    /// Open a new connection to the remote server.
    pub fn connect<A: ToSocketAddrs>(addr: A) -> Result<Client> {
        let stream = try!(TcpStream::connect(addr));
        Ok(Client { reader: BufReader::new(stream) })
    }

    pub fn hi(&mut self) -> Result<String> {
        try!(self.write_line("HI"));
        let reply = try!(self.read_line());
        Ok(reply)
    }

    /// Test the latency from the client to server to client.
    pub fn ping(&mut self) -> Result<PingLatency> {
        let timestamp_s = now().to_timespec().sec as u64;
        try!(self.write_line(&format!("PING {}", timestamp_s)));
        let start_time = now();

        let reply = try!(self.read_line());
        let latency = now() - start_time;
        if !reply.starts_with("PONG") { return Err(Error::InvalidServerResponse(reply)) }

        Ok(PingLatency { latency: latency })
    }

    /// Measure the time in seconds it takes to download the specified amount
    /// of bytes from the server.
    pub fn download(&mut self, size: usize) -> Result<SpeedMeasurement> {
        try!(self.write_line(&format!("DOWNLOAD {}", size)));
        try!(self.get_stream_mut().flush());
        let start_time = now();

        let reply = try!(self.read_line());
        let elapsed_time = now() - start_time;

        let size = reply.len();
        if size != size {
            return Err(Error::InvalidServerResponse(format!("Wrong size {}", size)))
        }

        Ok(SpeedMeasurement { size: size, duration: elapsed_time })
    }

    /// Measure the time in seconds it takes to upload the specified amount
    /// of bytes to the server.
    pub fn upload(&mut self, size: usize) -> Result<SpeedMeasurement> {
        let cmd_string = format!("UPLOAD {}", size);
        let cmd_size = cmd_string.len();

        let chunk_size = min(2048, size);
        let mut random_data_chunk = Vec::<u8>::with_capacity(chunk_size);
        for _ in 0..chunk_size {
            random_data_chunk.push(random::<u8>());
        }

        let mut missing = size - cmd_size - 2;

        try!(self.write_line(&cmd_string));
        let start_time = now();
        {
            let stream = self.get_stream_mut();

            while missing > 0 {
                let buf: &[u8] = if missing as isize - chunk_size as isize > 0 {
                    &random_data_chunk[..chunk_size]
                } else {
                    &random_data_chunk[..missing]
                };
                try!(stream.write(buf));
                missing -= buf.len();
            }
            try!(stream.write(b"\n"));
        }
        let reply = try!(self.read_line());

        let elapsed_time = now() - start_time;

        if !reply.starts_with(&format!("OK {size}", size = size)) {
            return Err(Error::InvalidServerResponse(reply))
        }

        Ok(SpeedMeasurement { size: size, duration: elapsed_time })
    }

    fn write_line(&mut self, string: &str) -> Result<()> {
        let stream = self.get_stream_mut();
        try!(
            stream.write((string.to_owned() + "\n").as_bytes())
            .map(|_| Ok(()))
        )
    }

    fn read_line(&mut self) -> Result<String> {
        let mut buf = String::new();
        try!(self.reader.read_line(&mut buf));
        Ok(buf)
    }

    fn get_stream_mut(&mut self) -> &mut TcpStream {
        self.reader.get_mut()
    }

    #[allow(dead_code)]
    fn flush(&mut self) -> Result<()> {
        try!(
            self.get_stream_mut().flush()
            .map(|_| Ok(()))
        )
    }
}
