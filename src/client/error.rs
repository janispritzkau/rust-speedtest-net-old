use std::io;
use std::time::SystemTimeError;

#[derive(Debug)]
pub enum Error {
    IoError(io::Error),
    SystemTimeError(SystemTimeError),
    InvalidServerResponse(String),
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Error {
        Error::IoError(error)
    }
}

impl From<SystemTimeError> for Error {
    fn from(error: SystemTimeError) -> Error {
        Error::SystemTimeError(error)
    }
}
