//! This crate provides an interface for testing connection speed by via
//! http://speedtest.net/ servers.

extern crate time;
extern crate rand;
extern crate rustc_serialize as serialize;
extern crate hyper as http;

pub mod client;
pub mod servers;
pub mod test;
