extern crate speedtest_net;

use speedtest_net::servers::{fetch_servers, get_best_server};

fn main() {
    let servers = fetch_servers().unwrap();
    println!("Fetched {} servers", servers.len());

    println!("Find the best based on ping ...");
    
    let server = get_best_server(&servers).unwrap();
    println!("Best server: {:?}", server);
}
