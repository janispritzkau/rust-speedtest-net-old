extern crate speedtest_net;

use speedtest_net::client::Client;

fn main() {
    let mut client = Client::connect("sp1.netkom-line.de:8080").unwrap();

    println!("[Ping]");
    for _ in 0..6 {
        println!("{:?}", client.ping().unwrap());
    }

    println!("[Download]");
    for _ in 0..4 {
        println!("{:?}", client.download(40000).unwrap());
    }

    println!("[Upload]");
    for _ in 0..4 {
        println!("{:?}", client.upload(40000).unwrap());
    }
}
